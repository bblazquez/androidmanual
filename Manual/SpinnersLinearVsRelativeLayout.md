# WIDGETS II, LinearLayout vs RelativeLayout
##WIDGETS
###Spinner
####Cargar y recuperar de la vista
```java
 final ArrayList<String> options = new ArrayList<>();
        options.add("Uno");
        options.add("Dos");
        options.add("Tres");
        options.add("Cuatro");
 Spinner spinner = (Spinner) findViewById(R.id.example_spinner);
 spinner.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item_example, options));
```
####Listener y recuperar valor
```java
      spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                 String value = options.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
```
####XML
```xml
   <Spinner
        android:id="@+id/example_spinner"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"/>
```
spinner_item_example.xml
```xml
<?xml version="1.0" encoding="utf-8"?>
	<TextView
    	android:id="@+id/data"
    	xmlns:android="http://schemas.android.com/apk/res/android"
    	android:layout_width="match_parent"
    	android:layout_height="wrap_content"
    	style="@style/spinner_style" />
```
styles.xml
```xml
    <style name="spinner_style">
        <item name="android:textSize" >18sp</item>
        <item name="android:textColor" >@color/black</item>
        <item name= "android:paddingLeft">10sp</item>
        <item name="android:paddingRight" >10sp</item>
    </style>
```

##LinearLayout Example
```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="horizontal"
    android:padding="10sp">

    <TextView
        android:id="@+id/description"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_weight="0.4"
        android:textSize="18sp"
        android:textStyle="bold"
        android:text="@string/title" />

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_weight="1"
        android:orientation="vertical">

        <TextView
            android:id="@+id/textview"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:gravity="center_horizontal"
            android:text="@string/article" />

        <EditText
            android:layout_width="match_parent"
            android:layout_height="wrap_content" />

    </LinearLayout>

</LinearLayout>
```

strings.xml
```xml
 <string name="title">Description of the main data:</string>
 <string name="article">Article</string>
```

##RelativeLayout Example
```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:padding="10sp">

    <TextView
        android:id="@+id/description"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="@string/title"
        android:textSize="18sp"
        android:textStyle="bold"
        android:layout_alignParentTop="true"
        android:layout_alignParentLeft="true"
        android:layout_alignParentStart="true" />

    <TextView
        android:id="@+id/textview"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/article"
        android:layout_marginRight="45dp"
        android:layout_marginEnd="45dp"
        android:layout_alignParentTop="true"
        android:layout_alignParentRight="true"
        android:layout_alignParentEnd="true" />

    <EditText
        android:id="@+id/edittext"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_below="@+id/textview"
        android:layout_alignLeft="@+id/textview"
        android:layout_alignStart="@+id/textview" />

</RelativeLayout>

```





