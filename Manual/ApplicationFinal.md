#APPLICATION + Final

##APPLICATION
La clase Application es la clase base que contiene el resto de elementos como activities o servicios. La clase Application es ejecutada antes que cualquier otra clase.
Su uso principal es la inicialización del estado global antes de que el primer Activity sea mostrado.
El uso de esta clase se recomienda en los siguientes casos:

* Tareas especiales a ser ejecutadas antes del primer activity.
* Inicialización global de variables que son compartidad entre todos los componentes.
* Intercambio de información mediante métodos estáticos.

**Aunque se pueda utilizar esta clase para el intercamio de información entre Activities, Fragments, Servicios etc... no está garantizado que vaya a figurar la información en memoria para siempre. Ya que con el paso del tiempo la clase será eliminada y reinstanciada, con lo que la información guardada se perderá.**

Se genera una nueva clase que extiende de Application, el nombre de esta clase debe coincidir con el campo android_name del AndroidManifest.xml

AndroidManifest.xml
```xml
	<?xml version="1.0" encoding="utf-8"?>
	<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="education.kernet.konectia">
    <uses-permission android:name="android.permission.INTERNET" />

    <application
        android:name=".KonectiaApp"
        android:allowBackup="true"
        ...
```

Clase KonectiaApp.java
```java
public class KonectiaApp extends Application {

	// Variables a ser compartidas por toda la aplicación
    // Puede ser un String, int, ArrayList ...
    // En este caso son dos objecto de los modelos Film y Album
    private Film workingFilm;
    private Album workingAlbum;

    @Override
    public void onCreate() {
        super.onCreate();
        
        // Precisamente en este punto es donde arranca la apliación
        // Aquí se introducirán aquellas tareas que se deban ejecutar en primer lugar
        ApplicationHolder.setApplication(this);
    }

    public Film getWorkingFilm() {
        return workingFilm;
    }

    public void setWorkingFilm(Film workingFilm) {
        this.workingFilm = workingFilm;
    }

    public Album getWorkingAlbum() {
        return workingAlbum;
    }

    public void setWorkingAlbum(Album workingAlbum) {
        this.workingAlbum = workingAlbum;
    }
}
```

Clase ApplicationHolder.java
Esta clase nos sirve para recoger la aplicación y por lo tanto la variables definidas en ella.
```java
public class ApplicationHolder {

    private static KonectiaApp application;

	// Método utlizado para recoger la aplicación en cualquier momento
    public static KonectiaApp getApplication() {
        return application;
    }

	// Método para setear una sola vez la aplicación. Utilizado en el punto anterior
    public static void setApplication(KonectiaApp application) {
        ApplicationHolder.application = application;
    }

}
```

###Ejemplo de uso
Sobre un activity
```java
	// Creamos una variable global de la clase
    private KonectiaApp app;
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Recuperamos la aplicación en el método on Create
        app = ApplicationHolder.getApplication();
    }
    
  albumListAdapter = new AlbumListAdapter(albumArrayList, this, new IOnAlbumClickListener() {
            @Override
            public void onItemClick(Album item) {
            
            // Cuando pulsamos un elemento de la lista seteamos ese item en la clase Application
                app.setWorkingAlbum(item);
            // Llamamos al siguiente activity
                callDetailsActivity();
            }
        });
```

Sobre otro activity
```java
	
 @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Recuperamos la aplicación
        KonectiaApp app = ApplicationHolder.getApplication();
        
        // Recuperamos el album anteriormente seteado
        Album album = app.getWorkingAlbum();
    }
```

##Final
###Documentación pendiente
####AsyncTask
Clase que permite realizar una tarea en segundo plano sin bloquear el hilo principal. Lo normal es utlizar el constructor para recibir parámetros del exterior. El méotodo onPreExecute se ejecuta antes de iniciar la tarea y por lo tanto es útil para arrancar un progressDialog. El método doInBackground trabaja en segundo plano y por lo tanto se puede usar para hacer llamadas a un servidor o tareas pesadas que bloquearían el hilo principal. Cuando la tarea ha terminado se ejecuta el méotodo onPostExecute donde eliminamos el progressDialog y ejecutamos lo que queramos teniendo en cuentas que volvemos a estar en el hilo principal.
Las AsyncTask llevan tres parámetros en su cabecera. Una buena explicación se encuentra en este link:
https://stackoverflow.com/questions/6053602/what-arguments-are-passed-into-asynctaskarg1-arg2-arg3
```java
	private class MyAsyncTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog;
        private Context context;

		// Constructor donde recibimos parámetros
        MyAsyncTask(Context context) {
            this.dialog = new ProgressDialog(context);
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Hilo principal, arrancamos el progressDialog
            dialog.setMessage(getString(R.string.checking_pending_tasks));
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
        	// Hilo secundario. Hacemos llamadas al servidor o tareas pesadas
            return null;
        }
        
        @Override
        protected void onPostExecute(Void aVoid) {
        	// Vuelta al hilo principal, eliminamos el progressDialog
            dialog.dismiss();
        }
    }
```

Para crear y arrancar la tarea asíncrona ejecutamos:
```java
        new MyAsyncTask(getActivity()).execute();
```


###TEMAS PENDIENTES
####Persistencia de datos
No se han impartido conocimientos relacionados con el guardado de información en el terminal. Por lo tanto es importante que el alumno interiorice conceptos como:

* Shared Preferences: La forma más fácil de guardar pequeñas cantidades de datos del usuario. https://developer.android.com/training/basics/data-storage/shared-preferences.html?hl=es-419
* Ficheros locales
* Bases de datos SQLite: Muy recomendable usar DBFlow ya que facilita mucho las cosas. https://agrosner.gitbooks.io/dbflow/content/

Resumen de peristencia de datos:
https://github.com/codepath/android_guides/wiki/Persisting-Data-to-the-Device

####Firebase
Permite crear bases de datos en el servidor desde la propia aplicación utilizando el servicio de google. Gratuito hasta un volumen de datos. Muy bien documentado y especialmente recomendable para aquellas personas que no puedan crear un servidor dedicado y que aún así deseen tener una base de datos en un servidor para almacenar y leer información.
https://firebase.google.com/docs/android/setup?hl=es-419


##Enlaces a documentación
Todas fotocopias impartidas se pueden encontrar en la siguiente dirección: https://bitbucket.org/bblazquez/androidmanual
Las fotocopias están creadas en markdown. Para poder visualizarlas una vez descargas recomiendo usar el programa Haroopad. Desde aquí podrá ser impreso en formato .pdf.

La aplicación que aúna los conocimientos dados durante el curso se puede encontrar en el siguiente proyecto git: https://bitbucket.org/bblazquez/androidkonectia

Mail del profesor para dudas: bblazquez@kernet.es
