# RECYCLERVIEW III + JSON

##RecyclerView III
Vamos a añadir un click listener para cada uno de los items. Además añadiremos y borraremos nuevos items dinámicamente.

###INTERFACE
IOnUserClick.java
Creamos la siguiente interfaz para comunicar al activity que se ha pulsado uno de los items:
```java
public interface IOnUserClick {
    void onItemClick(User user, int position);
}
```

###ACTIVITY
Hacemos una serie de modificaciones al activity principal:

* Pasamos al adapter la interfaz para detectar que se ha pulsado un item.
* Añadimos un FloatingActionButton. Será el encargado de lanzar un AlertDialog donde rellenaremos los campos para crear el nuevo usuario.
* Ejecutamos los métodos necesario para notificar al adapter que se ha añadido o eliminado un nuevo item a la lista.

	* Añadir item:
        * users.add(user);
        * mAdapter.notifyItemInserted(users.size() -1);
	* Eliminar item:
        * users.remove(user);
        * mAdapter.notifyItemRemoved(position);
        * mAdapter.notifyItemRangeChanged(position, users.size());
    
El Activity completo queda de la siguiente forma:
```java
public class MainActivity extends AppCompatActivity {
    private MyAdapter mAdapter;
    private ArrayList<User> users;

    private View dialogView;
    private AlertDialog.Builder alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // Cargamos la lista de usuarios que enviamos al adapter
        users = loadUsers();
        mAdapter = new MyAdapter(users, this, new IOnUserClick() {
            @Override
            public void onItemClick(User user, int position) {
                // Item de la lista pulsado
                Toast.makeText(MainActivity.this, user.getName(), Toast.LENGTH_SHORT).show();

                // Eliminamos del ArrayList y notificamos al adapter
                users.remove(user);
                mAdapter.notifyItemRemoved(position);
                mAdapter.notifyItemRangeChanged(position, users.size());
            }
        });
        mRecyclerView.setAdapter(mAdapter);

        initializeCreationDialog();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddDialog();
            }
        });
    }

    private void showAddDialog() {
        if (dialogView.getParent() != null) {
            ((ViewGroup) dialogView.getParent()).removeView(dialogView);
        }
        alertDialog.show();
    }

    private void initializeCreationDialog() {
        alertDialog = new AlertDialog.Builder(this);
        dialogView = getLayoutInflater().inflate(R.layout.user_creation_dialog, null);
        alertDialog.setView(dialogView);
        alertDialog.setCancelable(true);

        final EditText name = (EditText) dialogView.findViewById(R.id.edit_name);
        final EditText surname = (EditText) dialogView.findViewById(R.id.edit_surname);
        final EditText age = (EditText) dialogView.findViewById(R.id.edit_age);
        final EditText address = (EditText) dialogView.findViewById(R.id.edit_address);
        final EditText city = (EditText) dialogView.findViewById(R.id.edit_city);

        alertDialog.setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // Creamos un nuevo usuario
                User user = new User();
                user.setName(name.getText().toString());
                user.setSurname(surname.getText().toString());
                user.setAge(Integer.parseInt(age.getText().toString()));
                user.setAddress(address.getText().toString());
                user.setCity(city.getText().toString());

                // Añadimos el usuario al ArrayList y notificamos al adapter
                users.add(user);
                mAdapter.notifyItemInserted(users.size() -1);
            }
        });
        alertDialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    private ArrayList<User> loadUsers() {
        ArrayList<User> users = new ArrayList<>();
        User user1 = new User("Andrew", "Irvine", 10, "123 6th St.", "Melbourne");
        User user2 = new User("Reinhold", "Messner", 75, "44 Shirley Ave.", "West Chicago");
        User user3 = new User("Harish", "Kapadia", 40, "514 S. Magnolia St.", "Orlando");
        users.add(user1);
        users.add(user2);
        users.add(user3);
        return  users;
    }
}
```
<div style="page-break-after: always;"></div>


###ADAPTER
El adapter recibe la interfaz IOnUserClick que utilizará para avisar al Activity cuando se pulse unos de los items. Para ello creamos el método "bind" donde detectaremos la pulsación del item y avisaremos al activity.

```java
void bind(final User user, final int position, final IOnUserClick caller) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Notificamos item pulsado
                    caller.onItemClick(user, position);
                }
            });
        }
```
El adapter modificado queda de la siguiente forma.
```java
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private static ArrayList<User> users;
    private IOnUserClick caller;

    // Constructor donde recogemos información a mostrar en la lista
    public MyAdapter(ArrayList<User> users, Context context, IOnUserClick caller) {
        this.users = users;
        this.caller = caller;
    }

    // Referencia a la vista para cada uno de los elementos
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public TextView surnameTextView;
        public TextView addressTextView;
        public TextView cityTextView;
        public ImageView ageImageView;
        public ViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.name_textView);
            surnameTextView = (TextView) itemView.findViewById(R.id.surname_textView);
            addressTextView = (TextView) itemView.findViewById(R.id.address_textView);
            cityTextView = (TextView) itemView.findViewById(R.id.city_textView);
            ageImageView = (ImageView) itemView.findViewById(R.id.age_imageView);
        }

        // Método para detectar la pulsación sobre el item
        void bind(final User user, final int position, final IOnUserClick caller) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Notificamos item pulsado
                    caller.onItemClick(user, position);
                }
            });
        }
    }

    // Creación de nuevas vistas utilizando el ViewHolder superior
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item, parent, false);

        return new ViewHolder(view);
    }

    // Sobreescribimos la vista con el contenido del elemento
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = users.get(position);

        // Preparamos el método para detectar la pulsación
        holder.bind(user, position, caller);

        holder.nameTextView.setText(user.getName());
        holder.surnameTextView.setText(user.getSurname());
        holder.addressTextView.setText(user.getAddress());
        holder.cityTextView.setText(user.getCity());

        // Recogemos la edad y en función de ella cargamos una u otra imagen
        int age = user.getAge();
        if (age <= 15) {
            holder.ageImageView.setImageResource(R.drawable.son);
        } else if (age <= 70) {
            holder.ageImageView.setImageResource(R.drawable.man);
        } else {
            holder.ageImageView.setImageResource(R.drawable.grandfather);
        }
    }

    // Número de elementos añadidos
    @Override
    public int getItemCount() {
        return users.size();
    }

}

```
<div style="page-break-after: always;"></div>

##JSON
JSON, acrónimo de JavaScript Object Notation, es un formato de texto ligero para el intercambio de datos.
Los tipos de datos disponibles con JSON son:

* Números: Se permiten números negativos y opcionalmente pueden contener parte fraccional separada por puntos. Ejemplo: 123.456
* Cadenas: Representan secuencias de cero o más caracteres. Se ponen entre doble comilla y se permiten cadenas de escape. Ejemplo: "Hola"
* Booleanos: Representan valores booleanos y pueden tener dos valores: true y false
* null: Representan el valor nulo.
* Array: Representa una lista ordenada de cero o más valores los cuales pueden ser de cualquier tipo. Los valores se separan por comas y el vector se mete entre corchetes. Ejemplo ["juan","pedro","jacinto"]
* Objetos: Son colecciones no ordenadas de pares de la forma <nombre>:<valor> separados por comas y puestas entre llaves. El nombre tiene que ser una cadena y entre ellas. El valor puede ser de cualquier tipo. Ejemplo: {"departamento":8,"nombredepto":"Ventas","director": "juan rodriguez","empleados":[{"nombre":"Pedro","apellido":"Fernandez"},{"nombre":"Jacinto","apellido":"Benavente"} ]}

Objetivo: Parsear este JSON que reponde a la dirección: http://android.kernet.es/curso/ws/login.json
```JSON
{"id":"1","name":"Android","surname":"droid","email":"android@gmail.com","password":"12345"}
```

<div style="page-break-after: always;"></div>

###JAVA
Añadimos una nueva librería al fichero build.gradle(Module:app)
```gradle
dependencies {
    compile 'com.squareup.okhttp3:okhttp:3.3.1'
}
```

Creamos la clase HttpClient.java para gestionar las peticiones al servidor:
```java
public class HttpClient extends OkHttpClient {

    private OkHttpClient client;

    public HttpClient() {
        client = new OkHttpClient();
    }

    public Response get() throws Exception {

        // Preparamos la petición al servidor
        Request request;
        request = new Request.Builder()
                .url("http://android.kernet.es/curso/ws/login.json")
                .build();

        // Hacemos la llamada al servidor
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code " + response);
        }

        // Devolvemos la repuesta del servidor
        return response;
    }

}
```
<div style="page-break-after: always;"></div>

En el Activity, Fragment o donde queramos introducimos el siguiente método que utilizará la clase anterior para hacer una llamada al servidor.
```java
	private void attemptLogin() {
        // La petición al servidor no se debe hacer en el hilo principal
        // Por ello iniciamos un nuevo hilo
        new Thread(new Runnable() {
            @Override
            public void run() {
                Response response;
                HttpClient client = new HttpClient();
                try {
                    // Petición al servidor a través del objeto client
                    response = client.get();

                    // Parseamos la respuesta del servidor si es satisfactoria
                    if (response.isSuccessful() && !response.toString().isEmpty()) {
                        parseResponse(response);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

```
Parseamos la respuesta del servidor con el siguiente método
```java
    private void parseResponse(Response response) {
        try {
            // Recogemos el cuerpo de la respuesta en formato string
            String responseString = response.body().string();
            response.close();

            // Creamos un objecto JSON con la respuesta del servidor
            JSONObject jsonObject = new JSONObject(responseString);

            // Reocogemos cada elementos del JSON
            int id = jsonObject.getInt("id");
            String name = jsonObject.getString("name");
            String surname = jsonObject.getString("surname");
            String email = jsonObject.getString("email");
            String password = jsonObject.getString("password");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
```











