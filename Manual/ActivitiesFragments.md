# ACITIVITIES Y FRAGMENTS 
##Activity
####Definición
```android
	public class ExampleActivity extends AppCompatActivity {
```
####Recuperar XML
```android
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);
        
        EditText exampleEditText = (EditText) findViewById(R.id.example_editText);
    }
```
####XML
acitivty_example.xml
```xml
<LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical">
            <EditText
                    android:id="@+id/example_editText"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"/>
</LinearLayout>
```
####Manifest
```xml
    <activity
        android:name=".ExampleActivity"/>

```

##Fragment
####Definición (Importante seleccionar v4)
```android
import android.support.v4.app.Fragment;
	public class ExampleFragment extends Fragment {
```
####Recuperar  XML
```android
 @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.fragment_example, container, false);
    	TextView exampleTextView = (TextView) view.findViewById(R.id.example_textView);

    	return view;
    }

```
####XML
fragment_example.xml
```xml
<LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical">
            <TextView
                    android:id="@+id/example_textView"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"/>
</LinearLayout>
```

##Llamada ACTIVITY -> ACTIVITY
Nota: *Un Fragment también puede llamar a un Activity.*
### Sin parámetros
```android
	Intent myIntent = new Intent(**contexto, destinoActivity.class);
    startActivity(myIntent);
```
**contexto: Depende de donde estemos puede ser this, getApplicationContext() ...
### Con parámetros
Nota: *En el ejemplo se manda un String.*
#### Origen
```android
	Intent myIntent = new Intent(**contexto, destinoActivity.class);
    myIntent.putExtra("KEY", valorAEnviar);
    startActivity(myIntent);
```
#### Destino
```android
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destino);

        String valorRecibido = getIntent().getStringExtra("KEY");
    }
```
##Llamada ACTIVITY -> FRAGMENT
El Fragment se aloja en el FrameLayout del xml del Activity. Además tiene id=placeHolder para el ejemplo.
### Sin parámetros
####Llamada
```android
    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    ft.replace(R.id.placeHolder, new ExampleFragment());
    ft.commit();
```
#### XML del Acitivity que llama al Fragment
```xml
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <FrameLayout
        android:id="@+id/placeHolder"
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        </FrameLayout>

</LinearLayout>
```
### Con parámetros
El Fragment se aloja en el FrameLayout del xml del Activity. Además tiene id=placeHolder para el ejemplo.
#### Llamada
```android
	ExampleFragment exampleFragment = new ExampleFragment();
    Bundle args = new Bundle();
    args.putString("KEY", valorAEnviar);
    exampleFragment.setArguments(args);
    
    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    ft.replace(R.id.placeHolder, exampleFragment);
    ft.commit();
```
#### XML del Activity (Igual que el caso anterior)
#### Fragment destino
```android
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ...
        String valorRecibido = getArguments().getString("KEY", "");
    }
```
*Segundo parámetro de getString("KEY", ""); es "". Quiere decir que por defecto se devuelve vacío si no encuentra nada asociado a "KEY".



