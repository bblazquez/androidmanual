# MENUS
##MENUS
###XML
Se define un nuevo archivo xml de tipo menu. 
Para ello botón derecho sobre "res"-> New -> Android Resouce File -> Resource type -> Menu
Creamos fichero: menu_example.xml
```xml
<?xml version="1.0" encoding="utf-8"?>
<menu xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    tools:context=".activity.TabsActivity">

    <item
        android:id="@+id/action_settings"
        android:orderInCategory="100"
        android:title="Ajustes"
        app:showAsAction="never" />

    <item
        android:id="@+id/action_about"
        android:orderInCategory="100"
        android:title="Acerca de:"
        app:showAsAction="never" />

    <item
        android:id="@+id/action_icon"
        android:icon="@mipmap/ic_launcher"
        android:orderInCategory="100"
        android:title="Editando"
        android:visible="true"
        app:showAsAction="always" />
</menu>
```
###Activity
Añadimos en el manifest al activity correspondiente:
```XML
	android:theme="@style/Base.Theme.AppCompat.Light.DarkActionBar"
```
Tener en cuenta el appTheme en Styles resource:
```XML
    <style name="AppTheme" parent="Theme.AppCompat.Light.DarkActionBar">
        <!-- Customize your theme here. -->
        <item name="colorPrimary">@color/colorPrimary</item>
        <item name="colorPrimaryDark">@color/colorPrimaryDark</item>
        <item name="colorAccent">@color/colorAccent</item>
    </style>
```
Hacemos override de los siguientes métodos en el Activity:
```android
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_example, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Toast.makeText(this, "Ajustes", Toast.LENGTH_SHORT).show();
        }

        if (id == R.id.action_about) {
            Toast.makeText(this, "Acerca de", Toast.LENGTH_SHORT).show();
        }

        if (id == R.id.action_icon) {
            Toast.makeText(this, "Icono", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }
```