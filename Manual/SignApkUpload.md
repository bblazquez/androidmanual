#SIGN + DISTRIBUTE your APK

##SIGN APK
###BUILD VARIANTS (Tipos de compilaciones)
Información de: https://developer.android.com/studio/build/build-variants.html?hl=es-419
Cada variante de compilación representa una versión diferente de tu app que puedes compilar. Por ejemplo, es posible que desees compilar una versión de tu app que sea gratuita, con contenido limitado, y una versión paga que incluya más contenido.
Puedes crear y configurar tipos de compilación en el archivo build.gradle de nivel de módulo dentro del bloque android {}. Cuando creas un módulo nuevo, Android Studio crea automáticamente los tipos de compilación de depuración y lanzamiento.

* Depuración (Debug): Versión para que el desarrollador cree y analice su aplicación. Permite ver logs, debuguear, manejar excepciones... Mas lenta. Son firmadas por defecto.
* Lanzamiento (Release): Versión para distribución entre clientes. Necesitan ser firmadas con la keystore creada(Explicado en siguiente bloque)

####PRODUCT FLAVORS (Tipos de productos)
Sirven para crear versiones de una misma aplicación. Por ejemplo si para un tipo de usuarios queremos abrir una determinada URL y para otros otra.
Crear tipos de productos es similar a crear tipos de compilación: agrégalos al bloque productFlavors {} y configura los ajustes que desees.


Ejemplo de tipos de compilacines y productos. Cambios aplicados sobre el fichero build.gradle(Module:app)
```android
buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
        debug {
        }
    }

    productFlavors {

        pro {
        }
        normal {
        }
    }

}
```
<div style="page-break-after: always;"></div>

Después en el código podemos utlizar estos flavors para ejecutar unas u otras acciones:
```android
	if (BuildConfig.FLAVOR.equals("pro")) {
    	// Hacemos una acción para pro
    } else if (BuildConfig.FLAVOR.equals("normal")) {
        // Hacemos una acción para normal
    }
```

###SIGN APK
Android exige que todos los APK se firmen digitalmente con un certificado para poder instalarse.
Además:
```android
	"Debes firmar todos tus APK con el mismo certificado durante toda la vida útil prevista de tus aplicaciones. Al instalar una actualización en una aplicación, el sistema compara el o los certificados de la nueva versión con los de la versión existente. El sistema permite la actualización si los certificados concuerdan. Si firmas la nueva versión con un certificado diferente, debes asignar un nombre de paquete diferente a la aplicación. En este caso, el usuario instala la nueva versión como una aplicación completamente nueva."
```
Cuando firmas un APK, la herramienta de firma adjunta a este el certificado de clave pública. El certificado de clave pública sirve como una “huella digital” que asocia de manera exclusiva el APK contigo y con tu clave privada correspondiente. Esto permite a Android verificar que cualquier actualización futura a tu APK sea auténtica y provenga del autor original. La clave que se usa para la creación de este certificado se llama clave de firma de aplicaciones.

####Keystore + key
Keystore: Archivo .jks que permite administrar claves

Antes de firmar la aplicación se debe generar una <b>clave y un keystore</b>:
Desde el menú de Andoid Studio:

   1. En la barra de menú, haz clic en Build > Generate Signed APK.
   2. Selecciona un módulo del menú desplegable y haz clic en Next.
   3. Haz clic en Create new para crear una nueva clave y keystore.
   4. En la ventana New Key Store, proporciona la siguiente información para tu keystore y clave.

     Keystore
        * Key store path: selecciona la ubicación en la que se debe crear tu keystore.
        * Password: genera y confirma una contraseña segura para tu keystore.

    Key
        * Alias: ingresa un nombre de identificación para tu clave.
        * Password: genera y confirma una contraseña segura para tu clave. Esta debe ser diferente de la contraseña que elegiste para tu keystore.
        * Validity (years): fija el período de validez de tu clave en años. Tu clave debe ser válida durante al menos 25 años. Por ello, puedes firmar actualizaciones para tu aplicación con la misma clave durante toda su vida útil.
        * Certificate: ingresa información sobre ti para tu certificado. Esta información no se muestra en tu aplicación, pero se incluye en tu certificado como parte del APK.

    Una vez que completes el formulario, haz clic en OK.
    
   5. Continúa hacia Manually sign an APK si deseas generar un APK firmado con tu nueva clave, o haz clic en Cancel si solo deseas generar una clave y un keystore, sin firmar un APK.


####Manual sign
1. Haz clic en Build > Generate Signed APK para abrir la ventana Generate Signed APK. (Si ya generaste una clave y un keystore como se describe anteriormente, esta ventana estará abierta).

2. En la ventana Generate Signed APK Wizard, selecciona un keystore, una clave privada y escribe las contraseñas para ambos. (Si creaste tu keystore en la sección anterior, estos campos aparecerán completos). Luego, haz clic en Next.
3. En la ventana siguiente, selecciona un destino para el APK firmado, el tipo de compilación y (si corresponde) las clases de productos, y haz clic en Finish.

* *Signatures versions: In Android 7.0, APKs can be verified according to the APK Signature Scheme v2 (v2 scheme) or JAR signing (v1 scheme). Older platforms ignore v2 signatures and only verify v1 signatures.* 

Cuando se complete el proceso, encontrarás tu APK firmado en el archivo de destino que seleccionaste anteriormente. Ahora podrás distribuir tu APK firmado a través de un mercado de aplicaciones como Google Play Store o con el mecanismo que elijas.

##DISTRIBUTE APK
Puedes lanzar tus aplicaciones de Android de diferentes maneras. Generalmente, las aplicaciones se lanzan a través de un mercado de aplicaciones como Google Play, pero también puedes hacerlo en tu propio sitio web o enviarlas directamente a los usuarios.

Android brinda protección a los usuarios contra descargas e instalaciones inadvertidas de apps desde otras ubicaciones que no sean Google Play (confiable). Estas instalaciones se bloquean hasta que el usuario incluya fuentes desconocidas en Ajustes > Seguridad. Para permitir la instalación de aplicaciones desde otras fuentes, los usuarios deben realizar este cambio de configuración para poder descargar tus apps en sus dispositivos.

Para poder distribuir aplicaciones a través de Android Market hay que tener una cuenta "Google play publisher account". La cuenta se puede crear en el siguiente enlace y después de pagar **25 euros**. https://play.google.com/apps/publish/signup/ Con esta cuenta se pueden distribuir aplicaciones sin límite y la cuenta no caduca.

Para una descripción detallada de como subir una aplicación firmada al Android Market acceder al siguiente enlace: https://www.wikihow.tech/Submit-an-App-to-the-Android-Market
