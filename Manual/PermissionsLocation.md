# PERMISSIONS + Location Strategies

##PERMISSIONS
Los permisos son necesarios para ciertas características de una aplicación como conexiones remotas, geoposicionamiento, envío de mensajes...
Los permisos se incluyen en el manifest como figura en el siguiente ejemplo:

```android
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
          package="com.education.Konectia">

    <uses-permission android:name="android.permission.SEND_SMS"/>

    <application ...>
        ...
    </application>
</manifest>
```

Existen permisos que son potencialmente peligrosos para la privacidad del usuario como los que figuran en la siguiente lista:
https://developer.android.com/guide/topics/permissions/overview.html#permission-groups
Es por ello que los considerados como peligrosos deben ser aceptados por el usuario explicitamente.

En función de la API que esté utlizando el comportamiento es distinto:

* Android 6.0(API 23) o superior y además: targetSdkVersion 23 o mayor:
El usuario debe aceptar los permisos en tiempo de ejecución y no de instalación. En caso de que se pulse "Never ask again" no será necesario aceptar los permisos cuando la aplicación lo necesite.
El tratamiento de estos permisos a nivel de programación se especifica en el siguiente link:
https://developer.android.com/training/permissions/requesting.html
De momento chequearemos que el usuario no ha aceptado el permiso de la siguiente forma: (En el ejemplo final se muestra extendida la petición de permisos para posicionamiento)
```android
if (ContextCompat.checkSelfPermission(thisActivity, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
    // Permission is not granted
	}
```

* Android 5.1.1(API 22) o inferior.
El usuario debe aceptar los permisos en el momento de la instalacion.


##Location Strategies
Información recogida de https://developer.android.com/guide/topics/location/strategies.html
Lo primero que debemos hacer es dar permisos a la aplicación para que reciba la posición.
Hay dos tipos:

* ACCESS_COARSE_LOCATION: Acceso únicamente al NETWORK_PROVIDER.
* ACCESS_FINE_LOCATION: Accesso al NETWORK_PROVIDER + GPS_PROVIDER.

```android
<manifest ... >
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    ...
    <!-- Needed only if your app targets Android 5.0 (API level 21) or higher. -->
    <uses-feature android:name="android.hardware.location.gps" />
    ...
</manifest>
```
Diferencias entre NETWORK_PROVIDER y GPS_PROVIDER:

* NETWORK_PROVIDER: Si tus servicios de geo-localización son determinados por la red WiFi o móvil el consumo de batería es bajo, pero la exactitud de la posición no es tan confiable.
* GPS_PROVIDER: En cambio si tus servicios de geo-localización son determinados por el GPS, el consumo de batería es mayor comparado con el proveedor de red, pero la exactitud de la posición es mucho mejor.

El siguiente código muestra la utilización del LocationListener para calcular la posición:
```java
// Acquire a reference to the system Location Manager
LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

// Define a listener that responds to location updates
LocationListener locationListener = new LocationListener() {
    public void onLocationChanged(Location location) {
      // Called when a new location is found by the network location provider.
      makeUseOfNewLocation(location);
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {}

    public void onProviderEnabled(String provider) {}

    public void onProviderDisabled(String provider) {}
  };

// Register the listener with the Location Manager to receive location updates
locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
```
Como vemos en la última línea, en ésta se definen una serie parámetros que repercutirán al modo de captura de la posición:
 **locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);**

* Parámetro 1: NETWORK_PROVIDER o GPS_PROVIDER.
* Parámetro 2: Mínimo tiempo entre dos capturas.
* Parámetro 3: Mínima alteración de distancia entre dos capturas. Parámetros 2 y 3 a cero frecuencia máxima.
* Parámetro 4: Listener donde se recibe la posición.

Para recoger la última posición actualizada utilizamos lo siguiente:
```java
String locationProvider = LocationManager.NETWORK_PROVIDER;
// Or use LocationManager.GPS_PROVIDER

Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
```
En caso de que queramos parar el listener:
```java
// Remove the listener you previously added
locationManager.removeUpdates(locationListener);
```

###Código completo
```java
import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestPermissions();
    }

    private void requestPermissions() {
        // Chequeamos si el permiso está activo
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Permiso no activo
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                
                // Permiso anteriormente rechazado. // Preguntamos de nuevo
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

            } else {
                
                // Preguntamos por el permiso
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        } else {
            // Permiso anteriormete aceptado
            locationListener();
        }
    }

    private void locationListener() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                makeUseOfNewLocation(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

    private void makeUseOfNewLocation(Location location) {
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(location.toString());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // Respuesta del permiso solicitada
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationListener();
                } else {
                    Toast.makeText(this, "Permiso no aceptado", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
    
}
```