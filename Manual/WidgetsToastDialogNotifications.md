# WIDGETS, TOASTS, DIALOGS, NOTIFICATIONS
##WIDGETS
####TextView
```java
	TextView exampleTextView= (TextView) findViewById(R.id.example_textView);
    exampleTextView.setText("Valor");
```
####EditText
```java
	EditText exampleEditText= (EditText) findViewById(R.id.example_editText);
    String value = exampleEditText.getText().toString();
```
####CheckBox
```java
	CheckBox exampleCheckBox = (CheckBox) findViewById(R.id.example_checkBox);
    boolean value = exampleCheckBox.isChecked();
```
####Button
```java
	Button exampleButton = (Button) findViewById(R.id.example_button);
	exampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View view) {
            	//CODIGO
			}
		});
        
    exampleButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
          	public boolean onLongClick(View v) {
            	//CODIGO
             }
        });
```
####ImageView
```java
	ImageView exampleImage = (ImageView) findViewById(R.id.example_image);
	exampleImage.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View view) {
            	//CODIGO
			}
		});
```
####Ocultar/Mostrar Widget
```java
	textView.setVisibility(View.GONE);
    textView.setVisibility(View.VISIBLE);
```
##TOASTS
```java
	Context context = getApplicationContext();
	CharSequence text = "Hello toast!";
	int duration = Toast.LENGTH_SHORT;
    //int duration = Toast.LENGTH_LONG;
	Toast.makeText(context, text, duration).show();
```
##DIALOGS
###AlertDialog
```java
import android.support.v7.app.AlertDialog;

AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("T�tulo");
        builder.setMessage("Mensaje");
        builder.setCancelable(false); // Solo se cierra con ok o cancel
        builder.setPositiveButton("Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        //CODIGO
                    }
                });

        builder.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        //CODIGO
                    }
                });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
```
###CustomDialog
####Definici�n
```java
	AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
    View dialogView = getLayoutInflater().inflate(R.layout.custom_dialog_example, null);
    alertDialog.setView(dialogView);
    alertDialog.setCancelable(false);

    final EditText editText = (EditText)dialogView.findViewById(R.id.example_editText);

    alertDialog.setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        	//CODIGO
        }
    });
    alertDialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            //CODIGO
        }
    });
```
####Llamada
```java
	if (dialogView.getParent() != null) {
       ((ViewGroup) dialogView.getParent()).removeView(dialogView);
    }
    alertDialog.show();
```
####XML
```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:padding="20dp">

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:textSize="18sp"
        android:textStyle="bold"
        android:text="Nombre:"/>

    <EditText
        android:id="@+id/example_editText"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="18sp" />

</LinearLayout>
```
##NOTIFICATION
```java
   private void sendNotification(String title, String messageBody) {
        Intent intent = new Intent(this, TabsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        android.support.v4.app.NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_konectia)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(randomNotificationId(), notificationBuilder.build());
    }
    
    private int randomNotificationId() {
        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        return m;
    }
```
