### Configurar git
```
$ git config --global user.name "NOMBRE"
$ git config --global user.email NOMBRE@DOMINIO.COM
$ git config --global merge.tool [apt-get install diffuse]
$ git config --global color.ui true
$ git config --global core.pager cat
```

### Ignorar ficheros
 Fichero .gitignore para añadir elementos que no queremos tener sincronizados contra nuestro repositorio.


### Crear proyecto
```
$ mkdir proyecto
$ cd proyecto
$ git init
$ git remote add origin [URL_REPO_GIT]
```

### Clonar proyecto (por ejemplo para obtener un/nuestro proyecto de github):
```
$ git clone URL/PATH
```

### Operaciones de añadido y borrado
Añadido / modificado:
```
$ git add archivo(s)
```

Borrado:
```
$ git rm archivo(s)
```

### Inspeccionar repositorio
Lista los ficheros que estan en staged, unstaged, and untracked...
```
$ git status
```

Muestra el historial de commit con el formateo por defecto:
```
$ git log
```

Condensa cada entrada en una sóla línea:
```
$ git log --oneline
```

### Commit de nuestro código y push al repositorio remoto:
```
//Nuevos archivos:
$ git add [archivo(s)] //para todo --> .
$ git commit -m "comentario del commit"
$ git commit -p // Fichero a fichero...
```

```
//Commit normal para archivos existentes, modificados o borrados (no añadidos):
$ git commit -avm "comentario del commit"
$ git push origin master
```

### Actualizar cambios del proyecto (desde remoto):
```
$ git pull origin [rama]
[por ejemplo: git pull origin master]
// pull = fetch + merge
```

### Ramas
Consultar ramas:
```
$ git branch
```

Crear rama:
```
$ git branch [rama]
```

Borrar rama (en local):
```
$ git branch -d [rama]
```

Borrar rama (en remoto):
```
$ git push origin :[rama]
```

Cambiar de rama:
```
$ git checkout [rama]
```

Mezclar(Merge) una rama en la actual(en la que estamos situados):
```
$ git merge [otra rama]
```

### Tags
Crear Tag en estado actual del repositorio (HEAD de la rama)

```
$ git tag -a v1.0 -m 'version 1.0'
```
Revisamos el tag
```
$ git show v1.0
```
Para subir los tags al repositorio remoto
```
$ git push --tags`
```

Volver a un tag en concreto
```
$ git reset --hard tagname  // Reset al tag
$ git revert tagname
```

### Crear Tag en estado diferente al actual del repositorio

Buscamos el hash que queremos para el tag
```
$ git log --pretty=oneline
```

Elegimos el checksum y se lo metemos al comando de git tag
```
$ git tag -a v0.99 77eede7551644aa07b9c940f4c3c6934148abc38 -m "version 0.99"
```

### Stash

```
$ git stash {list / pop / apply }
```

### Workflows
http://blog.endpoint.com/2014/05/git-workflows-that-work.html

![workflow-master](./images/git_workflow_master.jpg)
![workflow-github](./images/git_workflow_github_flow.jpg)
![workflow-skullcandy](./images/git_workflow_skullcandy.jpg)
![workflow-gitflow](./images/git_workflow_gitflow.jpg)


### Misc.

Tutoriales integrados en el sistema:
man gittutorial / gittutorial-2
man gitworkflows

git help everyday (ayuda para los diferentes perfiles de desarrollo)

git revert ---> OK (Deshace un commit creando uno nuevo)
git reset ---> Dangerous revert :_D

git pull = git fetch + git merge


### URLs

http://rogerdudler.github.io/git-guide/
https://try.github.io
http://ndpsoftware.com/git-cheatsheet.html
https://www.atlassian.com/git/tutorials
https://git-scm.com/docs/
https://git-scm.com/book/en/v2
https://www.atlassian.com/git/tutorials/comparing-workflows
http://blog.endpoint.com/2014/05/git-workflows-that-work.html
