# SHORTCUTS + RECYCLERVIEW
##SHORTCUTS
|Descrición|Comando|
|----|:----:|
|Buscar en fichero|Control + F|
|Buscar en todos los ficheros del proyecto|Control + Shift + F|
|Buscar un  fichero|Control + Shift + N|
|Ir a la declaración|Control + B o Control + Click|
|Ayuda de Android|Alt + Insert|
|Guardar|Control + S|
|Compilar y ejecutar|Shift + F10|

##RecyclerView
Permite mostrar listas de datos basados en gran cantidad de información y que cambien frecuentemente. Pese a que existe también ListView se recomienda el uso de RecyclerView por ser su actualización.
https://developer.android.com/guide/topics/ui/layout/recyclerview.html
###GRADLE
Añadimos al fichero build.gradle(Module:.app) (Dentro de Gradle Scripts) la siguiente librería:
```gradle
    dependencies {
    	compile 'com.android.support:design:25.3.1'
    }
```
<div style="page-break-after: always;"></div>

###ACTIVITY
####XML
Situamos el RecyclerView para mostrar la lista
```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <android.support.v7.widget.RecyclerView
        android:id="@+id/my_recycler_view"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:scrollbars="vertical" />
</LinearLayout>
```
####JAVA
```java
public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        String[] strings = {"Uno", "Dos", "Tres"};
        mAdapter = new MyAdapter(strings);
        mRecyclerView.setAdapter(mAdapter);
    }
}
```
<div style="page-break-after: always;"></div>
###ADAPTER
####XML
my_text_view.xml
Cada uno de los items de la lista estará definido por my_text_view.xml
```xml
<?xml version="1.0" encoding="utf-8"?>
<TextView xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:textSize="20sp"
    android:textStyle="bold">
</TextView>
```
####JAVA
MyAdapter.java
```java
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private String[] mDataset;

    // Constructor donde recogemos información a mostrar en la lista
    public MyAdapter(String[] myDataset) {
        mDataset = myDataset;
    }

    // Referencia a la vista para cada uno de los elementos
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public ViewHolder(TextView v) {
            super(v);
            mTextView = v;
        }
    }

    // Creación de nuevas vistas utilizando el ViewHolder superior
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        TextView v = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_text_view, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Sobreescribimos la vista con el contenido del elemento
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTextView.setText(mDataset[position]);

    }

    // Número de elementos añadidos
    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
```

##RecyclerView II
Vamos a enriquecer el RecyclerView para mostrar más información en cada Item. Para ello vamos a crear una clase llamada User que usaremos para mostrar informacion en cada item de la lista.

###GRADLE
Añadimos al fichero build.gradle (Dentro de Gradle Scripts) la siguiente librería:
```gradle
    dependencies {
        compile 'com.android.support:cardview-v7:25.0.1'
    }
```
###ACTIVITY
####XML
Situamos el RecyclerView para mostrar la lista
```xml
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <android.support.v7.widget.RecyclerView
        android:id="@+id/my_recycler_view"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:padding="5dp"
        android:scrollbars="vertical" />
</LinearLayout>
```
<div style="page-break-after: always;"></div>
####JAVA
```java
public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        
        // Cargamos la lista de usuarios que enviamos al adapter
        ArrayList<User> users = loadUsers();
        
        mAdapter = new MyAdapter(users);
        mRecyclerView.setAdapter(mAdapter);
    }

    private ArrayList<User> loadUsers() {
        ArrayList<User> users = new ArrayList<>();
        User user1 = new User("Andrew", "Irvine", 10, "123 6th St.", "Melbourne");
        User user2 = new User("Reinhold", "Messner", 75, "44 Shirley Ave.", "West Chicago");
        User user3 = new User("Harish", "Kapadia", 40, "514 S. Magnolia St.", "Orlando");
        users.add(user1);
        users.add(user2);
        users.add(user3);
        return  users;
    }
}
```
<div style="page-break-after: always;"></div>
###Clase User
Creamos una nueva clase llamada User que usaremos para enviar información al adapter.
```java
public class User {
    private String name;
    private String surname;
    private int age;
    private String address;
    private String city;

    public User(String name, String surname, int age, String address, String city) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.address = address;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

```

###ADAPTER
Hacemos una serie de modificaciones al adapter para que trabaje con items complejos. Recoge la información de User.

####XML
user_item.xml
Cada uno de los items de la lista estará definido por user_item.xml
Ahora cada uno de los items es un CardView. Sirve para modelar cada elemento.
```xml
<?xml version="1.0" encoding="utf-8"?>

<android.support.v7.widget.CardView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginBottom="5dp"
    app:cardCornerRadius="2dp"
    app:cardUseCompatPadding="true">

    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:padding="5dp"
        android:orientation="vertical">

        <TextView
            android:id="@+id/name_textView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content" />

        <TextView
            android:id="@+id/surname_textView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:paddingLeft="5sp"
            android:layout_toRightOf="@id/name_textView" />

        <TextView
            android:id="@+id/address_textView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_below="@id/surname_textView" />

        <TextView
            android:id="@+id/city_textView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_below="@id/address_textView" />

        <ImageView
            android:id="@+id/age_imageView"
            android:layout_width="45sp"
            android:layout_height="45sp"
            android:layout_marginEnd="34dp"
            android:layout_marginRight="34dp"
            android:layout_centerVertical="true"
            android:layout_alignParentRight="true"
            android:layout_alignParentEnd="true" />

    </RelativeLayout>
</android.support.v7.widget.CardView>
```
####JAVA
MyAdapter.java
Adapter actualizado que muestra una imagen dependiendo del campo "age" del usuario al que hace referencia.
```java
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private ArrayList<User> users;

    // Constructor donde recogemos información a mostrar en la lista
    public MyAdapter(ArrayList<User> users) {
        this.users = users;
    }

    // Referencia a la vista para cada uno de los elementos
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public TextView surnameTextView;
        public TextView addressTextView;
        public TextView cityTextView;
        public ImageView ageImageView;
        public ViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.name_textView);
            surnameTextView = (TextView) itemView.findViewById(R.id.surname_textView);
            addressTextView = (TextView) itemView.findViewById(R.id.address_textView);
            cityTextView = (TextView) itemView.findViewById(R.id.city_textView);
            ageImageView = (ImageView) itemView.findViewById(R.id.age_imageView);
        }
    }

    // Creación de nuevas vistas utilizando el ViewHolder superior
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item, parent, false);

        return new ViewHolder(view);
    }

    // Sobreescribimos la vista con el contenido del elemento
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = users.get(position);
        holder.nameTextView.setText(user.getName());
        holder.surnameTextView.setText(user.getSurname());
        holder.addressTextView.setText(user.getAddress());
        holder.cityTextView.setText(user.getCity());

        // Recogemos la edad y en función de ella cargamos una u otra imagen
        int age = user.getAge();
        if (age <= 15) {
            holder.ageImageView.setImageResource(R.drawable.son);
        } else if (age <= 70) {
            holder.ageImageView.setImageResource(R.drawable.man);
        } else {
            holder.ageImageView.setImageResource(R.drawable.grandfather);
        }
    }

    // Número de elementos añadidos
    @Override
    public int getItemCount() {
        return users.size();
    }
}
```